# OpticalFlow #

Determines the video input's dominant directions of movement on the X and Y axis and repeatedly calls according URLs depending on the current direction.

## Build & Run ##

Go to your OpenFrameworks folder (If not installed see [here](http://openframeworks.cc/setup/linux-codeblocks/)).

```
#!shell

cd addons
git clone https://github.com/timscaffidi/ofxOpticalFlowFarneback.git
cd ../../apps/myapps
git clone git@bitbucket.org:dasantonym/OpticalFlow.git
cd OpticalFlow
make
make run
```

## HTTP Requests ##

The app sends a http request every nth frame (set this with ``HTTP_REQUEST_INTERVAL``) to ``http://localhost:3000``.

Paths are:

* ``/movement/left``
* ``/movement/right``
* ``/movement/up``
* ``/movement/down``