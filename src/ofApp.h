#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxOpticalFlowFarneback.h"
#include "ofxKinect.h"

#define INITIAL_CAMERA_WIDTH 640
#define INITIAL_CAMERA_HEIGHT 480

#define WIN_WIDTH 640
#define WIN_HEIGHT 480

#define VELOCITY_THRESHOLD 0.1
#define COUNT_MIN_VELOCITY 1.0
#define UPDATE_MEAN_INTERVAL 2

#define HTTP_REQUEST_INTERVAL 1
#define CONTINUOUS_HTTP_SEND true

#define VELOCITY_EASE 0.2

class ofApp : public ofBaseApp{
    
    int camWidth;
    int camHeight;
    int screenWidth;
    int screenHeight;
    
    ofVideoGrabber vidGrabber;
    ofxOpticalFlowFarneback opticalFlow;
    ofxCvColorImage capture;
    ofxCvGrayscaleImage gray;
    
    ofxKinect kinect;
    
    ofPoint mean;
    ofPoint meanTarget;
    bool drawInput;
    
    int dirX;
    int dirY;
    
    bool updatedX;
    bool updatedY;
    
    bool useKinect;
    
    unsigned int currentDeviceId;
    unsigned int numDevices;
    
public:
    void setup();
    void update();
    void draw();
    void setCamera();
    
    vector<std::string> getDeviceNames();
    
    void setupScreen();
    void keyPressed(int key);
    void windowResized(int w, int h);
    
};
