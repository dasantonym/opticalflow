#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    numDevices = 0;
    currentDeviceId = 0;
    drawInput = true;
    useKinect = false;
    mean = ofPoint(0.0, 0.0);
    meanTarget = ofPoint(0.0, 0.0);
    
    ofSetEscapeQuitsApp(false);
    setupScreen();
    ofEnableAlphaBlending();
    
    // enable depth->video image calibration
    kinect.setRegistration(true);
    
    kinect.init(true);
    //kinect.init(true); // shows infrared instead of RGB video image
    //kinect.init(false, false); // disable video image (faster fps)
    
    kinect.open();		// opens first available kinect
    //kinect.open(1);	// open a kinect by id, starting with 0 (sorted by serial # lexicographically))
    //kinect.open("A00362A08602047A");	// open a kinect using it's unique serial #
    
    // print the intrinsic IR sensor values
    if(kinect.isConnected()) {
        useKinect = true;
        opticalFlow.setup(kinect.width/2, kinect.height/2, 0.5, 3, 10, 1, 7, 1.5, false, false);
        ofLogNotice() << "sensor-emitter dist: " << kinect.getSensorEmitterDistance() << "cm";
        ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance() << "cm";
        ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize() << "mm";
        ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance() << "mm";
    }
}

//--------------------------------------------------------------
void ofApp::update()
{
    if (numDevices > 0 || useKinect)
    {
        if (vidGrabber.isInitialized() || useKinect)
        {
            if (useKinect)
            {
                kinect.update();
            } else {
                vidGrabber.update();
            }
            if (useKinect)
            {
                if (kinect.isFrameNew())
                {
                    gray.setFromPixels(kinect.getPixels(), kinect.width, kinect.height);
                    capture = gray;
                }
                else
                {
                    return;
                }
            }
            else if (vidGrabber.isFrameNew())
            {
                capture.setFromPixels(vidGrabber.getPixels(), camWidth, camHeight);
                gray = capture;
                gray.brightnessContrast(0.75, 0.6);
                capture = gray;
            }
            else
            {
                return;
            }
            /*
            gray = capture;
            gray.contrastStretch();
            gray.blur(3);
            gray.threshold(100);
             */
            opticalFlow.update(capture);
            ofPoint sum = ofPoint(0.0, 0.0);
            int counted = 0;
            
            for (int y = 0; y<camHeight; y+=1)
            {
                for (int x = 0; x<camWidth; x+=1)
                {
                    ofPoint vel = opticalFlow.getVelAtPixel(x, y);
                    if (fabs(vel.x) > COUNT_MIN_VELOCITY && fabs(vel.y) > COUNT_MIN_VELOCITY)
                    {
                        sum.x += vel.x;
                        sum.y += vel.y;
                        counted += 1;
                    }
                }
            }
            if (ofGetFrameNum() % UPDATE_MEAN_INTERVAL == 0)
            {
                if (counted > 0)
                {
                    meanTarget = ofPoint(sum.x/counted, sum.y/counted);
                }
                else
                {
                    meanTarget = ofPoint(0.0, 0.0);
                }
            }
            
            if (fabs(meanTarget.x-mean.x) > VELOCITY_THRESHOLD || fabs(meanTarget.y-mean.y) > VELOCITY_THRESHOLD)
            {
                mean = ofPoint(mean.x+(meanTarget.x-mean.x)*VELOCITY_EASE, mean.y+(meanTarget.y-mean.y)*VELOCITY_EASE);
            }
            
            if (ofGetFrameNum() % HTTP_REQUEST_INTERVAL == 0) {
                if (mean.x < VELOCITY_THRESHOLD*-1.0)
                {
                    if (dirX >= 0)
                    {
                        dirX = -1;
                        if (!CONTINUOUS_HTTP_SEND)
                        {
                            ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/left");
                        }
                    }
                }
                else if (mean.x > VELOCITY_THRESHOLD)
                {
                    if (dirX <= 0)
                    {
                        dirX = 1;
                        if (!CONTINUOUS_HTTP_SEND)
                        {
                            ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/right");
                        }
                    }
                }
                else
                {
                    dirX = 0;
                }
                
                if (mean.y < VELOCITY_THRESHOLD*-1.0)
                {
                    if (dirY >= 0)
                    {
                        dirY = -1;
                        if (!CONTINUOUS_HTTP_SEND)
                        {
                            ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/up");
                        }
                    }
                }
                else if (mean.y > VELOCITY_THRESHOLD)
                {
                    if (dirY <= 0)
                    {
                        dirY = 1;
                        if (!CONTINUOUS_HTTP_SEND)
                        {
                            ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/down");
                        }
                    }
                }
                else
                {
                    dirY = 0;
                }
            }
            if (CONTINUOUS_HTTP_SEND)
            {
                switch (dirX)
                {
                    case -1:
                        ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/left");
                        break;
                    case 1:
                        ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/right");
                        break;
                    default:
                        break;
                }
                switch (dirY)
                {
                    case -1:
                        ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/up");
                        break;
                    case 1:
                        ofLoadURLAsync("http://127.0.0.1:4000/api/v2/casimir/down");
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(0,0,0);
    ofSetHexColor(0xffffff);
    if (numDevices == 0 && !useKinect)
    {
        ofDrawBitmapString("NO INPUT - PRESS C TO SELECT CAM", 20.0, 25.0);
    }
    else
    {
        if (drawInput)
        {
            capture.draw(0.0, 0.0, screenWidth, screenHeight);
            
            if (opticalFlow.isInitialized())
            {
                opticalFlow.drawColored(screenWidth, screenHeight, 10, 3);
            }
        }
        
        ofSetHexColor(0x000000);
        ofFill();
        ofRect(0.0, 0.0, screenWidth, 30.0);
        ofNoFill();
        ofSetHexColor(0xffffff);
        
        stringstream m;
        m << "FPS " << ofGetFrameRate() << endl;
        ofDrawBitmapString(m.str(), screenWidth-100.0, 20.0);
        
        switch (dirX)
        {
            case -1:
                ofDrawBitmapString("LEFT", 60.0, 20.0);
                break;
            case 1:
                ofDrawBitmapString("RIGHT", 60.0, 20.0);
                break;
            default:
                break;
        }
        
        switch (dirY)
        {
            case -1:
                ofDrawBitmapString("UP", 20.0, 20.0);
                break;
            case 1:
                ofDrawBitmapString("DOWN", 20.0, 20.0);
                break;
            default:
                break;
        }
    }
}

void ofApp::setCamera()
{
    vector<ofVideoDevice> devices = vidGrabber.listDevices();
    numDevices = devices.size();
    if (numDevices > 0)
    {
        if (currentDeviceId >= numDevices)
        {
            currentDeviceId = 0;
        }
        if (vidGrabber.isInitialized())
        {
            vidGrabber.close();
        }
        vidGrabber.setVerbose(true);
        if (devices[currentDeviceId].bAvailable)
        {
            unsigned int formatSize = devices[currentDeviceId].formats.size();
            unsigned int width = 0;
            unsigned int height = 0;
            for (unsigned int n = 0; n < formatSize; ++n)
            {
                if (devices[currentDeviceId].formats[n].width >= 640 && devices[currentDeviceId].formats[n].width <= 1280 && width == 0)
                {
                    width = devices[currentDeviceId].formats[n].width;
                    height = devices[currentDeviceId].formats[n].height;
                }
            }
            if (width == 0)
            {
                width = INITIAL_CAMERA_WIDTH;
                height = INITIAL_CAMERA_HEIGHT;
            }
            
            vidGrabber.setDeviceID(devices[currentDeviceId].id);
            vidGrabber.initGrabber(width, height);
            
            camWidth = int(vidGrabber.getWidth());
            camHeight = int(vidGrabber.getHeight());
            
            opticalFlow.setup(vidGrabber.getWidth()/2, vidGrabber.getHeight()/2, 0.5, 3, 10, 1, 7, 1.5, false, false);
        }
    }
}

vector<std::string> ofApp::getDeviceNames()
{
    vector<ofVideoDevice> devices = vidGrabber.listDevices();
    vector<std::string> deviceNames;
    for (unsigned int i = 0; i < numDevices; ++i)
    {
        deviceNames.push_back(devices[i].deviceName);
    }
    return deviceNames;
}

void ofApp::setupScreen()
{
    int mode = ofGetWindowMode();
    if(mode == OF_WINDOW)
    {
        screenWidth = WIN_WIDTH;
        screenHeight = WIN_HEIGHT;
    }
    else if(mode == OF_FULLSCREEN)
    {
        screenWidth = ofGetScreenWidth();
        screenHeight = ofGetScreenHeight();
    }
    else if (mode == OF_GAME_MODE)
    {
        screenWidth = ofGetScreenWidth();
        screenHeight = ofGetScreenHeight();
    }
}



//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch (key)
    {
        case 'c':
            setCamera();
            break;
        case 'd':
            drawInput = !drawInput;
            break;
        default:
            break;
    }
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
    if ((w != WIN_WIDTH || h != WIN_HEIGHT) && ofGetWindowMode() == OF_FULLSCREEN)
    {
        setupScreen();
    }
    else
    {
        ofSetWindowShape(WIN_WIDTH, WIN_HEIGHT);
        setupScreen();
    }
}


