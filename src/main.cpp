#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGLFWWindow.h"

#define WIN_WIDTH 640
#define WIN_HEIGHT 480

//========================================================================
int main( ){
    ofAppGLFWWindow window;
#ifdef linux
    ofSetupOpenGL(&window, WIN_WIDTH,WIN_HEIGHT,OF_WINDOW);
#else
    ofSetupOpenGL(&window, WIN_WIDTH,WIN_HEIGHT,OF_WINDOW);			// <-------- setup the GL context
#endif

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());

}
